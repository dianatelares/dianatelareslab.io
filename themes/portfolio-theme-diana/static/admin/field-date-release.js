export default {
	label: 'Release Date',
	name: 'date_release',
	widget: 'date',
	require: true,
	default: '',
	format: 'YYYY-MM-DD',
	hint: 'When was this released to the public? This is used to order items one after an other in time (newest first).'
}
