export default {
	label: 'Youtube Video ID',
	name: 'video',
	widget: 'string',
	required: false,
	hint: 'To find the Youtube video ID, copy it from the Youtube Video URL you would like to use. Ex: it is `U5VCVp414f8` in the youtube URL https://www.youtube.com/watch?v=U5VCVp414f8&otherThing=test. So all the part between `?v=` and the next `&`'
}
