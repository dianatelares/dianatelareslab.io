import patternPath from './pattern-path.js'

export default {
	label: 'ID',
	name: 'id',
	widget: 'string',
	hint: 'The ID, is the unique IDentifier for this item. It should be unique among all items, and is used when associating two items together. Like an Article to a Collection.',
	pattern: patternPath
}
