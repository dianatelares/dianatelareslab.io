import gallery from './field-gallery.js'
import video from './field-video.js'
import radio4000Slug from './field-radio4000-slug.js'

const configs = {
	name: 'configs',
	label: 'Config',
	editor: {
		preview: false
	},
	files: [
		{
			name: "config.toml",
			label: "config.toml",
			file: "config.toml",
			fields: [
				{
					label: 'Title',
					name: 'title',
					widget: 'string'
				},
				{
					label: 'Base URL',
					name: 'baseURL',
					widget: 'string'
				},
				{
					label: 'Language Code',
					name: 'languageCode',
					widget: 'string'
				},
				{
					label: 'Theme',
					name: 'theme',
					widget: 'select',
					options: ['portfolio-theme-diana']
				},
				{
					label: 'Enable Git Info',
					name: 'enableGitInfo',
					widget: 'hidden'
				},
				{
					label: 'goldmark Unsafe Html',
					name: 'markup.ooldmark.renderer.unsafe',
					widget: 'hidden'
				},
				{
					label: 'Menu',
					name: 'menu',
					widget: 'object',
					fields: [
						{
							label: 'Main',
							name: 'main',
							widget: 'list',
							fields: [
								{
									label: 'Name',
									name: 'name',
									required: true,
									widget: 'string'
								},
								{
									label: 'Url',
									name: 'url',
									required: true,
									widget: 'string'
								},
								{
									label: 'Title',
									name: 'title',
									required: true,
									widget: 'string'
								},
								{
									label: 'Weight',
									name: 'weight',
									required: true,
									widget: 'number',
									valueType: 'int',
									min: 1,
									max: 100
								}
							]
						}
					]
				}
			]
		},
		{
			label: 'Homepage',
			name: 'homepage',
			file: 'data/homepage.yml',
			fields: [
				video,
				gallery,
				radio4000Slug
			]
		}
	]
}

export default configs
