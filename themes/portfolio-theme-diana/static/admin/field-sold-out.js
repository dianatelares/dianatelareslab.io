export default {
	label: "Sold Out",
	name: "sold_out",
	widget: "boolean",
	required: false,
	default: false,
	hint: 'Select this field if your article is "sold out". It means there will never be more stock. If there will be stock, use the in-stock field to say that there are no stock, instead of sold out.',
};
