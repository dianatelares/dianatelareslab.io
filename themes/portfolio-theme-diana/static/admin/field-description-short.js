export default {
	label: 'Short Description',
	name: 'description_short',
	widget: 'text',
	required: false,
	hint: 'Write a short description about this item. It wil be used when briefly presenting an overview of this item.'
}
