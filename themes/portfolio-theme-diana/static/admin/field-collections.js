export default {
	label: 'Collection',
	name: 'collection',
	widget: 'relation',
	collection: 'collections',
	searchFields: ['title', 'id'],
	valueField: 'id',
	displayFields: ['title'],
	hint: 'To which collection belong this item?'
}
