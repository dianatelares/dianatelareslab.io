export default {
	label: 'Show in menu',
	name: 'menu',
	widget: 'select',
	required: false,
	default: 'main',
	hint: 'If this field is selected, this item will be showed in the main menu of this site',
	options: [
		{
			label: 'Yes',
			value: 'main'
		},
		{
			label: 'No',
			value: ''
		}
	]
}
