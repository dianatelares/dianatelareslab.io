import id from './field-id.js'
import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import dateRelease from './field-date-release.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'
import gallery from './field-gallery.js'
import descriptionShort from './field-description-short.js'

const collections	 = {
	format: 'toml-frontmatter',
	name: 'collections',
	label: 'Collections',
	label_singular: 'Collection',
	folder: 'content/collections',
	create: true,
	slug: '{{title}}',
	editor: {
		preview: false
	},
	fields: [
		id,
		title,
		slug,
		draft,
		dateRelease,
		featuredImage,
		gallery,
		body
	]
}

export default collections
