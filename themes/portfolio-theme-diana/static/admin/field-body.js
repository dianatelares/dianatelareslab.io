export default {
	label: 'Description',
	name: 'body',
	widget: 'markdown',
	required: false,
	hint: 'Write a description about this item. Free text and medias.'
}
