export default {
	label: 'Featured Image',
	name: 'image',
	widget: 'image',
	required: false,
	hint: 'Select which image will be use to describe this item.'
}
