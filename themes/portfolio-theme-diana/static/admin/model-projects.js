import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import dateRelease from './field-date-release.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'
import gallery from './field-gallery.js'

const projects	= {
	format: 'toml-frontmatter',
	name: 'projects',
	label: 'Projects',
	label_singular: 'Project',
	folder: 'content/projects',
	create: true,
	slug: '{{title}}',
	editor: {
		preview: false
	},
	fields: [
		title,
		slug,
		draft,
		dateRelease,
		featuredImage,
		gallery,
		body
	]
}

export default projects
