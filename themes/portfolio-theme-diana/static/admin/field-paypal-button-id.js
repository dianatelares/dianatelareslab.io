export default {
	label: 'Paypal Button ID',
	name: 'paypal',
	widget: 'string',
	required: false,
	hint: 'The ID, is the unique IDentifier for this articals\'s Paypal Button. You can find it in the related button on paypal.com/buttons Be sure to copy the ID of the button, you can find it in the code that paypal offers you for the button. To show a Paypal button, your product also needs to have the field "In stock" activated.'
}
