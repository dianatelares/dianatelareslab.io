import collections from "./collections.js";

export default {
	config: {
		// Skips config.yml.
		// By not skipping, the configs will be merged, with the js-version taking priority.
		load_config_file: false,
		display_url: window.location.origin,

		backend: {
			name: "gitlab",
			auth_type: "pkce",
			branch: "main",
			repo: "dianatelares/dianatelares.gitlab.io",
			app_id:
				"9e2cb5b843c30efb4a8e7aa91b86f77b7e0c7c2e895b5c46646f11e86417182d",
		},

		media_folder: "static/media/uploads",
		public_folder: "media/uploads",

		collections,
	},
};
