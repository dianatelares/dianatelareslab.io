export default {
	label: 'Gallery',
	name: 'gallery',
	widget: 'list',
	required: false,
	hint: 'A list of images to present this item.',
	fields: [
		{
			label: 'Image',
			name: 'image',
			widget: 'image'
		},
		{
			label: 'Caption',
			name: 'caption',
			widget: 'string',
			required: false
		}
	]
}
