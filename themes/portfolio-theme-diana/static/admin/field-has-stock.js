export default {
	label: "In stock",
	name: "has_stock",
	widget: "boolean",
	required: false,
	default: false,
	hint: "Select this field if your article is in stock, if the product is available. This means that the visitors of the site can order it throught Paypal. You should be sur to also have create a related product/button on paypal.com/buttons.",
};
