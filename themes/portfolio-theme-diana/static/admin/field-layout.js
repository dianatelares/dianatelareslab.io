export default {
	label: 'Visual layout',
	name: 'layout',
	required: true,
	widget: 'select',
	hint: 'The layout, is which visual template will be used on this page. It defines the appearence and functionnality of the pageon the website.',
	options: ['', 'no-title']
}
