export default {
	label: 'Page Theme',
	name: 'theme',
	widget: 'select',
	required: false,
	default: '',
	hint: 'If this field is selected, this item will be showed in the main menu of this site',
	options: [
		{
			label: 'Default page',
			value: ''
		},
		{
			label: 'Simple, no title',
			value: 'single-simple'
		}
	]
}
