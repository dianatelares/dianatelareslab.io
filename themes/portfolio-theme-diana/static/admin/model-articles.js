import title from "./field-title.js";
import dateRelease from "./field-date-release.js";
import price from "./field-price.js";
import soldOut from "./field-sold-out.js";
import slug from "./field-slug.js";
import sizes from "./field-sizes.js";
import draft from "./field-draft.js";
import fieldCollections from "./field-collections.js";
import body from "./field-body.js";
import featuredImage from "./field-featured-image.js";
import gallery from "./field-gallery.js";
import hasStock from "./field-has-stock.js";
import paypalButtonId from "./field-paypal-button-id.js";

const articles = {
	format: "toml-frontmatter",
	name: "articles",
	label: "Articles",
	label_singular: "Article",
	folder: "content/articles",
	create: true,
	slug: "{{title}}",
	editor: {
		preview: false,
	},
	fields: [
		title,
		slug,
		draft,
		dateRelease,
		price,
		paypalButtonId,
		hasStock,
		soldOut,
		sizes,
		fieldCollections,
		featuredImage,
		gallery,
		body,
	],
};

export default articles;
