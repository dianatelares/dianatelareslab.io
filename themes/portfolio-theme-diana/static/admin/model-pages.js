import title from "./field-title.js";
import slug from "./field-slug.js";
import draft from "./field-draft.js";
import body from "./field-body.js";
import layout from "./field-layout.js";
import showInMenu from "./field-show-in-menu.js";

const pages = {
	name: "pages",
	label: "Pages",
	format: "toml-frontmatter",
	label_singular: "Page",
	create: true,
	folder: "content/",
	editor: {
		preview: false,
	},
	fields: [title, slug, draft, layout, showInMenu, body],
};

export default pages;
