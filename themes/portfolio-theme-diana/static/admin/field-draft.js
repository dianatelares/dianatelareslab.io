export default {
	label: "Draft",
	name: "draft",
	widget: "boolean",
	required: false,
	default: false,
	hint: "If set to 'true', this content will not be visible on the live website (only visible in admin).",
};
