class JavascriptCheck extends HTMLElement {
	constructor() {
		super();
		document.querySelector("body").setAttribute("has-javascript", true);
	}
}

customElements.define("javascript-check", JavascriptCheck);

export default JavascriptCheck;
