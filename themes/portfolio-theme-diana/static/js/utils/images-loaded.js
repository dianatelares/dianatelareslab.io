const template = document.createElement("template");

template.innerHTML = `
	<style>
		:host([hidden]) { display: none }
		:host {
		}
	</style>
	<div class="Component"></div>
`;

class ImagesLoaded extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.$component = this.shadowRoot.querySelector(".Component");
	}

	connectedCallback() {
		this.imgs = document.querySelectorAll("img");
		Object.entries(this.imgs).forEach((img) => {
			let i = img[1];
			if (i.complete || i.naturalHeight !== 0) {
				i.setAttribute("loaded", true);
			} else {
				i.addEventListener("load", this.handleLoaded);
			}
		});
	}
	handleLoaded = (event) => {
		event.target.setAttribute("loaded", true);
		event.target.removeEventListener("onload", this.handleLoaded);
	};
}

customElements.define("images-loaded", ImagesLoaded);

export default ImagesLoaded;
