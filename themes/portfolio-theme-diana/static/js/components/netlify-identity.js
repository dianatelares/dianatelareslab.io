/*
	 - only insert the netlify identity script if it is required;
	 - it checks on presence of the same URL params used by netlify
 */

class NetlifyIdentity extends HTMLElement {
	scriptUrl = "https://identity.netlify.com/v1/netlify-identity-widget.js";

	/* URL params (routes), are defined here:
		 https://github.com/netlify/netlify-identity-widget/blob/master/src/netlify-identity.js#L154 */
	routes = /(confirmation|invite|recovery|email_change)_token=([^&]+)/;
	errorRoute = /error=access_denied&error_description=403/;
	accessTokenRoute = /access_token=/;

	constructor() {
		super();
	}

	connectedCallback() {
		const hash = (document.location.hash || "").replace(/^#\/?/, "");
		if (!hash) {
			return;
		}

		const m = hash.match(this.routes);
		const em = hash.match(this.errorRoute);
		const am = hash.match(this.accessTokenRoute);
		if (m || em || am) {
			this.render();
		}
	}

	render() {
		let s = document.createElement("script");
		s.setAttribute("src", this.scriptUrl);
		document.querySelector("body").appendChild(s);
	}
}

customElements.define("netlify-identity", NetlifyIdentity);

export default NetlifyIdentity;
