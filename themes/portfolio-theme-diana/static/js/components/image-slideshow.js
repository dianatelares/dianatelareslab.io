class ImageSlideshow extends HTMLElement {
	connectedCallback() {
		this.onclick = this.handleClick.bind(this);
	}

	/* scroll the horizontal slideshow */
	handleClick(event) {
		const screenCenterWidth = window.innerWidth / 2;
		const screenCenterHeight = window.innerHeight / 2;

		const clickPositionMovement = () => {
			if (event.clientX > screenCenterWidth) {
				return (event.clientX - screenCenterWidth) * 1.5;
			} else {
				return (screenCenterWidth - event.clientX) * -1.5;
			}
		};

		this.scrollTo({
			top: 0,
			left: this.scrollLeft + clickPositionMovement(),
			behavior: "smooth",
		});
	}
}

customElements.define("image-slideshow", ImageSlideshow);

export default ImageSlideshow;
