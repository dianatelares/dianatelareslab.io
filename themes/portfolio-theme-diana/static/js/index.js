/* import and insert in page DOM */
import ImagesLoaded from "./utils/images-loaded.js";
import JavascriptCheck from "./utils/javascript-check.js";
import NetlifyIdentity from "./components/netlify-identity.js";

/* import and defined until used in templates */
import ImageSlideshow from "./components/image-slideshow.js";

/* insert what's needed in the DOM */
const $body = document.querySelector("body");
$body.appendChild(document.createElement("javascript-check"));
$body.appendChild(document.createElement("images-loaded"));
$body.appendChild(document.createElement("netlify-identity"));

export default {};
