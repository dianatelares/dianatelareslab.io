+++
id = "Gentry2018"
title = "Gentry 2018"
date_release = "2019-11-11T00:00:00.000Z"
image = "/media/uploads/dscf7715.jpg"

[[gallery]]
image = "/media/uploads/deatras.jpg"

[[gallery]]
image = "/media/uploads/dscf7645.jpg"

[[gallery]]
image = "/media/uploads/dscf7747.jpg"

[[gallery]]
image = "/media/uploads/dscf7750.jpg"

[[gallery]]
image = "/media/uploads/gentry9.png"

[[gallery]]
image = "/media/uploads/1-copia-2.jpg"

[[gallery]]
image = "/media/uploads/dscf7761.jpg"

[[gallery]]
image = "/media/uploads/dscf7715.jpg"

[[gallery]]
image = "/media/uploads/perfil3.jpg"

[[gallery]]
image = "/media/uploads/open.jpg"

[[gallery]]
image = "/media/uploads/dscf3544.jpg"

[[gallery]]
image = "/media/uploads/dscf7732.jpg"

[[gallery]]
image = "/media/uploads/dscf7528.jpg"

[[gallery]]
image = "/media/uploads/dscf8736-copia.jpg"

[[gallery]]
image = "/media/uploads/dscf3492-1-.jpg"

[[gallery]]
image = "/media/uploads/dscf3540.jpg"

[[gallery]]
image = "/media/uploads/dscf3515.jpg"

[[gallery]]
image = "/media/uploads/defrentee.jpg"
+++
Gentry is a project born at Muchafibra Coworking, Barcelona in 2017. Its main source of inspiration is the constant stimulus generated by the fast pace of a large multicultural city. Based on observation of urban movement and the transformation of its spaces with the socio-cultural consequences that they have. Gentry has, and is still developing, a series of different backpack collections, each inspired in new scenarios, innovating both the designs and the fabrics used. The last collection took place in Quetzaltenango, Guatemala.

Triana Núñez de Prado collected and shot the pictures for this project. @anaalcubo
