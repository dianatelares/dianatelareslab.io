+++
date_release = "2019-12-11"
id = "collections"
image = "/media/uploads/virgen.jpg"
title = "Cobán"

[[gallery]]
  image = "/media/uploads/dscf2690.jpg"

[[gallery]]
  image = "/media/uploads/dscf2673.jpg"

[[gallery]]
  image = "/media/uploads/dscf2642.jpg"

[[gallery]]
  image = "/media/uploads/dscf2713.jpg"

[[gallery]]
  image = "/media/uploads/dscf2734.jpg"

[[gallery]]
  image = "/media/uploads/dscf5097-2-2bb.jpg"

[[gallery]]
  image = "/media/uploads/dscf5365.jpg"

[[gallery]]
  image = "/media/uploads/dscf2726.jpg"

[[gallery]]
  image = "/media/uploads/dscf2611.jpg"

[[gallery]]
  image = "/media/uploads/dscf5122.jpg"

[[gallery]]
  image = "/media/uploads/dscf5114.jpg"

[[gallery]]
  image = "/media/uploads/dscf2763.jpg"

[[gallery]]
  image = "/media/uploads/dscf2839.jpg"

[[gallery]]
  image = "/media/uploads/dscf2812.jpg"

[[gallery]]
  image = "/media/uploads/dscf2923.jpg"

[[gallery]]
  image = "/media/uploads/dscf5295.jpg"

[[gallery]]
  image = "/media/uploads/dscf5309.jpg"

[[gallery]]
  image = "/media/uploads/dscf5419.jpg"

+++
Coban is the capital city of Alta Verapaz, located in the north of Guatemala. Here there is a textile technique passed down over the years from grandmother to granddaughter. It is particular to this area, born from the observation of spiders weaving their webs. On the waist loom, they weave a slim breathable white cotton fabric, similar to gauze, with natural motifs like leaves, dogs or the feminine-masculine duality, representing the spirituality and wisdom of their culture. 

The fabric for our collection was handcrafted with pedal looms by weavers Elvira Lopez and Roman Maldonado, both from Sibilia, with a black and white base color scheme embroidered on blue, red and beige tones inspired by the Mediterranean nature. The Coban textile is combined with some others that we delicately choose from the area. The clothes were designed by Diana, and sewn one by one carefully in association with Pixan, based in Quetzaltenango. The result is a limited handcrafted collection of dresses, jackets and some other comfortable, feminine, delicate and, at the same time, durable clothing pieces available in different sizes. Several of the collection’s models are available to be sent by post or in Shibalba design store in Antigua Guatemala.

We will be glad to answer any of your questions at the following e-mail address.

 dianatelares@gmail.com
