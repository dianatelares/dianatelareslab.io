+++
title = "Margarita B&W"
slug = "margarita-BW"
date_release = "2020-04-12"
price = 90
paypal = "53AEMFBTFJ68A"
sold_out = true
sizes = ["m", "l"]
collection = "collections"
image = "/media/uploads/dscf2713.jpg"

[[gallery]]
image = "/media/uploads/dscf5057.jpg"

[[gallery]]
image = "/media/uploads/dscf5058.jpg"

[[gallery]]
image = "/media/uploads/dscf2713.jpg"

[[gallery]]
image = "/media/uploads/dscf2722.jpg"

[[gallery]]
image = "/media/uploads/dscf2740.jpg"
+++
The Margarita dress is a midi confy dress confectioned with the Coban cotton gauze fresh fabric, and combines black and white in the embroidery. It is open in the front with three concha nacar buttons, side interior pockets and a scooped back neck. Handmade in Guatemala and sewed by Margarita Guzmán, who gives de name to the piece, throught [Pixan](https://www.pixan.design/) association.

There are three pieces produced in this model.

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress.
