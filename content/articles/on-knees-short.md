+++
title = "70 € Elvia on black"
slug = "elvia-on-black"
date_release = "2020-04-04"
price = 75
paypal = "FC4M99GQAEYVG"
has_stock = true
sizes = ["s", "m", "l"]
collection = "collections"
image = "/media/uploads/dscf2865.jpg"

[[gallery]]
image = "/media/uploads/dscf2869.jpg"

[[gallery]]
image = "/media/uploads/dscf2839.jpg"

[[gallery]]
image = "/media/uploads/dscf2877.jpg"

[[gallery]]
image = "/media/uploads/dscf2880.jpg"

[[gallery]]
image = "/media/uploads/c18.jpg"

[[gallery]]
image = "/media/uploads/c11.jpg"
+++
The Elvia dress is a confy short and fresh dress confectioned with a Coban gauze black cotton fabric embroided in red, sitting just above the knee, open in the front with three red metal buttons, with side interior pockets and a scooped back neck. Handmade in Guatemala, through [Pixan](https://www.pixan.design/) association.

There are three units produced.

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress.
