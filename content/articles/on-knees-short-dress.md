+++
title = "70 € Elvia on white "
slug = "elvia-on-white"
date_release = "2020-04-04"
price = 75
paypal = "Q93C82CCLGEBY"
has_stock = true
sizes = ["m"]
collection = "collections"
image = "/media/uploads/dscf2749.jpg"

[[gallery]]
image = "/media/uploads/dscf2763.jpg"

[[gallery]]
image = "/media/uploads/dscf2749.jpg"

[[gallery]]
image = "/media/uploads/dscf2766.jpg"

[[gallery]]
image = "/media/uploads/dscf2820.jpg"

[[gallery]]
image = "/media/uploads/dscf2812.jpg"
+++
The Elvia dress is a confy short and fresh dress elaborated with a Coban gauze white cotton fabric embroided in blue, sitting just above the knee, open in the front with three concha nacar buttons, with side interior pockets and a scooped back neck. Handmade produced in Guatemala, through[ Pixan](https://www.pixan.design/) association.

There are three units produced. 

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress.
