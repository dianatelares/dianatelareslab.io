+++
title = "Kimo Jaspe Brown"
slug = "kimo-jaspe-brown"
draft = true
date_release = "2020-04-06"
price = 60
paypal = "QXCAZBPK8WABL"
has_stock = true
sizes = ["m"]
collection = "collections"
image = "/media/uploads/s0015155b.jpg"

[[gallery]]
image = "/media/uploads/dscf5204.jpg"

[[gallery]]
image = "/media/uploads/s0015155b.jpg"

[[gallery]]
image = "/media/uploads/dscf2611.jpg"

[[gallery]]
image = "/media/uploads/dscf5197.jpg"
+++
The Kimo is a piece to wear as a dress or as a blázer, elaborated with a Jaspe/Ikat black white brown fabric, from the Salcajá market Guatemalan altiplan. It goes opened in the front with metal buttons, has japanese sleeves combining with Coban black fabric, has a belt to tie at the waist, sitting just above the knee, with side interior pockets. Handmade produced in Guatemala, through [Pixan](https://www.pixan.design/) association.

It is a unique model produced.

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress
