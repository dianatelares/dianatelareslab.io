+++
title = "A Sololá"
draft = true
date_release = "2018-04-02"
price = 80
paypal = ""
sold_out = true
collection = "GentryonGuatemala"
image = "/media/uploads/16.jpg"

[[gallery]]
image = "/media/uploads/15.jpg"

[[gallery]]
image = "/media/uploads/16.jpg"

[[gallery]]
image = "/media/uploads/17.jpg"

[[gallery]]
image = "/media/uploads/18.jpg"

[[gallery]]
image = "/media/uploads/25.jpg"

[[gallery]]
image = "/media/uploads/22.jpg"

[[gallery]]
image = "/media/uploads/11.jpg"
+++
Daily square backpack, elaborated with a red cotton textile, which is woven on a waist loom, originating in Sololá, Guatemala. It has four pockets, 3 auxiliary (one on the top, one on the front and another on the side) and the main one. With adjustable handles. Handmade in Guatemala, throught [Pixan](https://www.pixan.design/) association.

\    Size: 30 cm width, 30 cm heigh, 11 cm depth\
    Volume : 20 L

Unique Model produced.
