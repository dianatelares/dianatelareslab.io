+++
title = "Kimo Jaspe purple"
slug = "kimo-jaspe-purple"
draft = true
date_release = "2020-04-09"
price = 60
paypal = "2D9QCCP4JHKL8"
has_stock = true
sizes = ["m"]
collection = "collections"
image = "/media/uploads/dscf5450-2.jpg"

[[gallery]]
image = "/media/uploads/dscf5445.jpg"

[[gallery]]
image = "/media/uploads/dscf5450-2.jpg"

[[gallery]]
image = "/media/uploads/dscf2726.jpg"

[[gallery]]
image = "/media/uploads/dscf2728.jpg"

[[gallery]]
image = "/media/uploads/dscf5497-2b.jpg"

[[gallery]]
image = "/media/uploads/dscf5486.jpg"

[[gallery]]
image = "/media/uploads/dscf5492.jpg"
+++
<iframe width="560" height="315" src="https://www.youtube.com/embed/RDvcH4yJB1g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The Kimo is a piece to wear as a dress or as a blázer, elaborated with a Jaspe/Ikat multicolor black white purple fabric, from the Salcajá market Guatemalan altiplan. It goes opened in the front with metal buttons, has japanese sleeves combining with Coban black fabric, has a belt to tie at the waist, sitting just above the knee, with side interior pockets. Handmade produced in Guatemala, through [Pixan](https://www.pixan.design/) association.

It is a unique model produced.

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress
