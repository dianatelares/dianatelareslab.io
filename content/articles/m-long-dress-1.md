+++
collection = "collections"
date_release = "2020-04-12"
has_stock = true
image = "/media/uploads/dscf2554.jpg"
paypal = "JVPRTE2AQJYS4"
price = 90
sizes = ["m"]
slug = "margarita-white-and-yellow"
title = "Margarita white and Yellow"

[[gallery]]
  image = "/media/uploads/dscf5114.jpg"

[[gallery]]
  image = "/media/uploads/dscf5122.jpg"

[[gallery]]
  image = "/media/uploads/c33.jpg"

+++
The Margarita dress is a midi white dress elaborated with a Cobán cotton gauze fresh fabric, combined with a white and yellow Ikat fabric in the top. It is open in the front with three concha nacar buttons, side interior pockets and a scooped back neck. Handmade in Guatemala and sewed by Margarita Guzmán, who gives de name to the piece, throught the [Pixan](https://www.pixan.design/) asociation.

Unique model production

We are delighted to answer any questions, help with the size guide and take your orders at dianatelares@gmail.com adress.
